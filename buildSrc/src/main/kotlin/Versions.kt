/*
 * Copyright (c) 2018-2020 Pavel Vasin
 *
 * Licensed under the Jelurida Public License version 1.1
 * for the Blacknet Public Blockchain Platform (the "License");
 * you may not use this file except in compliance with the License.
 * See the LICENSE.txt file at the top-level directory of this distribution.
 */

object Versions {
    const val kotlin = "1.4.10"
    const val coroutines = "1.3.9"
    const val serialization = "1.0.0"
    const val ktor = "1.4.1"
    const val weupnp = "0.1.4"
    const val bouncycastle = "1.68"
    const val leveldbjni = "1.18.3"
}
